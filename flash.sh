#!/bin/bash
set -e

cargo +stable build --release

output_dir="target/thumbv7em-none-eabihf/release"
output_name="gdvkey-firmware"

arm-none-eabi-size -d target/thumbv7em-none-eabihf/release/gdvkey-firmware | (
	read line
	echo $line
	read line
	echo $line | sed -r ':L;s=\b([0-9]+)([0-9]{3})\b=\1K=g;t L'
) | column -t -s' '

arm-none-eabi-objcopy -O binary "$output_dir/$output_name" "$output_dir/$output_name.bin"
st-flash write "$output_dir/$output_name.bin" 0x8000000
