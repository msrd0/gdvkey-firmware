#![no_std]
#![no_main]

use panic_halt as _;

use core::{cell::RefCell, convert::TryInto};
use cortex_m::{
	interrupt::{free as interrupt_free, Mutex},
	peripheral::NVIC
};
use cortex_m_rt::entry;
use lazy_static::lazy_static;
use stm32l4::stm32l4x2::{interrupt, Interrupt, Peripherals, GPIOA, LPTIM1, LPTIM2, USART2};

mod setup;

const CLOCK_SPEED: u32 = 24000000; // 24 MHz
const UART_BAUD: u32 = 9600;
const KEYSCAN_FREQ: u16 = 1;
const LEDUPD_FREQ: u16 = 2;

lazy_static! {
	static ref MUTEX_GPIOA: Mutex<RefCell<Option<GPIOA>>> = Mutex::new(RefCell::new(None));
	static ref MUTEX_LPTIM1: Mutex<RefCell<Option<LPTIM1>>> = Mutex::new(RefCell::new(None));
	static ref MUTEX_LPTIM2: Mutex<RefCell<Option<LPTIM2>>> = Mutex::new(RefCell::new(None));
	static ref MUTEX_USART2: Mutex<RefCell<Option<USART2>>> = Mutex::new(RefCell::new(None));
	static ref LED_INTERRUPT: Mutex<RefCell<bool>> = Mutex::new(RefCell::new(false));
	static ref LED_ON: Mutex<RefCell<bool>> = Mutex::new(RefCell::new(false));
}

fn usart2_write(c: char) {
	let code: u8 = u32::from(c).try_into().unwrap();
	interrupt_free(|cs| {
		let usart2 = MUTEX_USART2.borrow(cs).borrow();
		match usart2.as_ref() {
			Some(usart2) => usart2.tdr.write(|w| unsafe { w.tdr().bits(code.into()) }),
			_ => {}
		}
	});
}

fn blink_led() {
	interrupt_free(|cs| {
		let gpioa = MUTEX_GPIOA.borrow(cs).borrow();
		let led_on = LED_ON.borrow(cs).borrow();
		match gpioa.as_ref() {
			Some(gpioa) => {
				if led_on.clone() {
					gpioa.bsrr.write(|w| w.br5().set_bit());
				} else {
					gpioa.bsrr.write(|w| w.bs5().set_bit());
				}
			},
			_ => {}
		};
		LED_INTERRUPT.borrow(cs).replace(true);
	});
}

#[entry]
fn main() -> ! {
	let p = Peripherals::take().unwrap();
	setup::clock(&p);
	setup::timer(&p);
	setup::uart(&p);
	setup::i2c(&p);

	// set PA5 as output
	p.GPIOA.moder.modify(|_, w| w.moder5().output());

	// move required peripherals into static variables
	interrupt_free(|cs| {
		MUTEX_GPIOA.borrow(cs).replace(Some(p.GPIOA));
		MUTEX_LPTIM1.borrow(cs).replace(Some(p.LPTIM1));
		MUTEX_LPTIM2.borrow(cs).replace(Some(p.LPTIM2));
		MUTEX_USART2.borrow(cs).replace(Some(p.USART2));
	});

	// enable interrupts
	unsafe {
		NVIC::unmask(Interrupt::LPTIM1);
		NVIC::unmask(Interrupt::LPTIM2);
	}

	loop {
		interrupt_free(|cs| {
			let led_interrupt = LED_INTERRUPT.borrow(cs).borrow();
			if led_interrupt.clone() {
				let led_on = LED_ON.borrow(cs).borrow().clone();
				LED_ON.borrow(cs).replace(!led_on);
			}
		});

		// go to sleep
		cortex_m::asm::wfi();
	}
}

#[interrupt]
fn LPTIM1() {
	interrupt_free(|cs| {
		let lptim1m = MUTEX_LPTIM1.borrow(cs).borrow();
		let lptim1 = lptim1m.as_ref().unwrap();
		if lptim1.isr.read().arrm().bit() {
			lptim1.icr.write(|w| w.arrmcf().set_bit());
		}
	});

	usart2_write('!');
}

#[interrupt]
fn LPTIM2() {
	interrupt_free(|cs| {
		let lptim2m = MUTEX_LPTIM2.borrow(cs).borrow();
		let lptim2 = lptim2m.as_ref().unwrap();
		if lptim2.isr.read().arrm().bit() {
			lptim2.icr.write(|w| w.arrmcf().set_bit());
		}
	});

	blink_led();
}
