use super::{CLOCK_SPEED, KEYSCAN_FREQ, LEDUPD_FREQ, UART_BAUD};
use core::convert::TryInto;
use stm32l4::stm32l4x2::Peripherals;

pub fn clock(p: &Peripherals) {
	let pwr = &p.PWR;
	let rcc = &p.RCC;

	// pwr control
	rcc.apb1enr1.modify(|_, w| w.pwren().set_bit());

	// leave reset state
	pwr.cr1.modify(|_, w| w.dbp().set_bit());

	// enable LSE
	rcc.bdcr.modify(|_, w| w.lseon().set_bit());
	while !rcc.bdcr.read().lserdy().bit() {}

	// enable HSI
	rcc.cr.modify(|_, w| w.hsion().set_bit());
	while !rcc.cr.read().hsirdy().bit() {}

	// enable 24 MHz PLL using 12 MHz HSE: 12 / 1(PLLM) * 8(PLLN) / 4(PLLR) = 24
	// also, PLLQ (can be used for USB) will have a clock of 48 MHz: 12/1*8 / 2(PLLQ) = 48
	rcc.pllcfgr.modify(|_, w| unsafe {
		w
		.pllsrc().bits(0b11) // 00: no clock, 01: MSI, 10: HSI, 11: HSE
		.pllm().bits(0) // 1
		.plln().bits(8)
		.pllr().bits(1) // 4
    	.pllq().bits(0) // 2
		.pllren().set_bit()
		// .pllq().bits(0) // 2
		// .pllqen().set_bit()
	});
	rcc.cr.modify(|_, w| w.pllon().set_bit());
	while !rcc.cr.read().pllrdy().bit() {}

	// select PLL as system clock source
	rcc.cfgr.modify(|_, w| unsafe { w.sw().bits(3) });
	while rcc.cfgr.read().sw().bits() != 3 {}

	// we don't need the MSI anymore
	rcc.cr.modify(|_, w| w.msion().clear_bit());

	// with a clock speed <26MHz, we can enter range 2
	pwr.cr1.modify(|_, w| unsafe { w.vos().bits(2) });

	// the STM32L452RET chip exposes GPIO A, B and C
	rcc.ahb2enr
		.modify(|_, w| w.gpioaen().set_bit().gpioben().set_bit().gpiocen().set_bit());
}

pub fn timer(p: &Peripherals) {
	let rcc = &p.RCC;
	let lptim1 = &p.LPTIM1;
	let lptim2 = &p.LPTIM2;

	// enable LPTIM1/2 which will be our main source of interrupts based on LSE
	rcc.ccipr.modify(|_, w| unsafe { w.lptim1sel().bits(3).lptim2sel().bits(3) });
	rcc.apb1enr1.modify(|_, w| w.lptim1en().set_bit());
	rcc.apb1enr2.modify(|_, w| w.lptim2en().set_bit());

	// set prescaler to 8: 32768 / 8 = 4096 should be sufficient
	lptim1.cfgr.modify(|_, w| unsafe { w.presc().bits(0b011) });
	lptim2.cfgr.modify(|_, w| unsafe { w.presc().bits(0b011) });

	// interrupt on timer auto reload (before enable)
	lptim1.ier.modify(|_, w| w.arrmie().set_bit());
	lptim2.ier.modify(|_, w| w.arrmie().set_bit());

	// enable both timers
	lptim1.cr.modify(|_, w| w.enable().set_bit());
	lptim2.cr.modify(|_, w| w.enable().set_bit());

	// set the auto reload register to the key scan frequency
	lptim1.arr.write(|w| {
		let cnt = (4096 + KEYSCAN_FREQ / 2) / KEYSCAN_FREQ;
		unsafe { w.arr().bits(cnt) }
	});
	while !lptim1.isr.read().arrok().bit() {}
	lptim1.icr.write(|w| w.arrokcf().set_bit());

	// set the auto reload register to the led update frequency
	lptim2.arr.write(|w| {
		let cnt = (4096 + LEDUPD_FREQ / 2) / LEDUPD_FREQ;
		unsafe { w.arr().bits(cnt) }
	});
	while !lptim2.isr.read().arrok().bit() {}
	lptim2.icr.write(|w| w.arrokcf().set_bit());

	// start the counter
	lptim1.cr.modify(|_, w| w.cntstrt().set_bit());
	lptim2.cr.modify(|_, w| w.cntstrt().set_bit());
}

pub fn uart(p: &Peripherals) {
	let rcc = &p.RCC;
	let gpioa = &p.GPIOA;
	let usart1 = &p.USART1;

	// the USART1 is the only one exposed
	gpioa.moder.modify(|_, w| w.moder9().alternate().moder10().alternate());
	gpioa.afrh.modify(|_, w| w.afrh9().af7().afrh10().af7());
	rcc.apb2enr.modify(|_, w| w.usart1en().set_bit());
	usart1
		.brr
		.write(|w| w.brr().bits(((CLOCK_SPEED + UART_BAUD / 2) / UART_BAUD).try_into().unwrap()));
	usart1.cr1.modify(|_, w| w.te().set_bit());
	usart1.cr1.modify(|_, w| w.ue().set_bit());
}

pub fn i2c(p: &Peripherals) {
	let rcc = &p.RCC;
	let gpiob = &p.GPIOB;
	let i2c1 = &p.I2C1;
	let i2c2 = &p.I2C2;

	// the I2C1 is connected to the lower LED driver
	gpiob.moder.modify(|_, w| w.moder6().alternate().moder7().alternate());
	gpiob.afrl.modify(|_, w| w.afrl6().af4().afrl7().af4());
	rcc.apb1enr1.modify(|_, w| w.i2c1en().set_bit());
	i2c1.cr1.modify(|_, w| w.pe().clear_bit()); // disable
	i2c1.cr1.modify(|_, w| w.anfoff().clear_bit().dnf().filter4()); // filtering
	i2c1.timingr.modify(|_, w| {
		w.presc()
			.bits(0)
			.scldel()
			.bits(11)
			.sdadel()
			.bits(3)
			.sclh()
			.bits(17)
			.scll()
			.bits(35)
	}); // timings
	i2c1.cr1.modify(|_, w| w.nostretch().clear_bit()); // must be cleared for master
	i2c1.cr1.modify(|_, w| w.pe().set_bit()); // enable

	// the I2C2 is connected to the lower LED driver
	gpiob.moder.modify(|_, w| w.moder6().alternate().moder7().alternate());
	gpiob.afrh.modify(|_, w| w.afrh13().af4().afrh14().af4());
	rcc.apb1enr1.modify(|_, w| w.i2c2en().set_bit());
	i2c2.cr1.modify(|_, w| w.pe().clear_bit()); // disable
	i2c2.cr1.modify(|_, w| w.anfoff().clear_bit().dnf().filter4()); // filtering
	i2c2.timingr.modify(|_, w| {
		w.presc()
			.bits(0)
			.scldel()
			.bits(11)
			.sdadel()
			.bits(3)
			.sclh()
			.bits(17)
			.scll()
			.bits(35)
	}); // timings
	i2c2.cr1.modify(|_, w| w.nostretch().clear_bit()); // must be cleared for master
	i2c2.cr1.modify(|_, w| w.pe().set_bit()); // enable
}
